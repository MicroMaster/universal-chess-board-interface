#include "Gui_protocol.h"
#include "Ucb.h"

using namespace std;

ofstream flog("ucb.log", ofstream::trunc);

void usage(char *progname)
{
    cout << APPLICATION_NAME << endl;
    cout << "Usage: " << progname << " <options>" << endl;
    cout << "\twhere <options> are:" << endl;
    cout << "\t\t-h\t\t\tTo get this help." << endl;
    cout << "\t\t-u <serial_port>\tpath to the serial port to use. By default /dev/ttyUSB0 is used." << endl;
    cout << "\t\t-v\t\t\tShows the program version number" << endl;
    exit(0);
}


int main(int argc, char* argv[])
{
    Gui_protocol gui;
    Ucb board;
    int c;

    gui.set_application_name(APPLICATION_NAME);
    gui.set_output(&cout);
    board.set_interface(&gui);

    flog << endl << endl << APPLICATION_NAME << endl; flog.flush();

    board.initialize("/dev/ttyUSB0");

    while ((c = getopt(argc, argv,"hu:v")) != -1)
    {
        switch (c)
        {
        case 'h':
            usage(argv[0]);
            break;
        case 'u':
            board.initialize(string(optarg));
            break;
        case 'v':
            cout << APPLICATION_NAME << endl;
            exit(0);
        default:
            usage(argv[0]);
            break;
        }
    }

    gui.send(APPLICATION_NAME);

    try
    {
        setbuf(stdin, NULL);
        do
        {
            char in_buff[BUFF_SIZE];
            string input, to_ucb;

            if (!gui.is_xboard_protocol() && !gui.is_uci_protocol())
                cout << "> ";
            if(fgets(in_buff, BUFF_SIZE, stdin)==NULL)
            {
                flog << "stdin Error." << endl; flog.flush();
                flog.close();
                exit(-1);
            }
            string str_buff(in_buff);
            input = str_buff.substr(0,str_buff.find('\n'));
            flog << "From stdin: " << input << endl; flog.flush();

            to_ucb = gui.interpret(input);
            if (to_ucb!="")
                board.send(to_ucb);
        }
        while(!gui.exit());

        cout << "XBOARD connection closed." << endl; cout.flush();
        flog << "XBOARD connection closed." << endl; flog.flush();
        board.wait();
        flog.close();
    }
    catch ( const std::exception & e )
    {
        flog << "ERROR: " << e.what() << endl; flog.flush();
        flog.close();
        std::cerr << e.what();
        return -1;
    }
    return 0;
}
