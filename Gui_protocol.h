#ifndef XBOARD_PROTOCOL_H
#define XBOARD_PROTOCOL_H

#include <algorithm>
#include <fstream>
#include <iostream>
#include <boost/type_traits.hpp>
#include <boost/token_functions.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>


using namespace std;
using namespace boost;

extern ofstream flog;

class Gui_protocol
{
    public:
        Gui_protocol(void);
        ~Gui_protocol();

        void        set_output(ostream*);
        void        set_application_name(const string&);
        void        send(const string&);
        bool        is_xboard_protocol(void);
        bool        is_uci_protocol(void);
        string      interpret(const string &);
        bool        exit(void);

    private:
        ostream     *output;
        bool        is_exit;
        bool        xboard_f=false;
        bool        uci_f=false;
        string      app_name;

        void        tokenize(const string&, vector<string>&);
        string      fen_to_ucb(const string&);
        string      fen_to_ucb2(const string&);
        string      beep(void);
        bool        is_move(const string*);
        string      interpret_xboard(const vector<string> &);
        string      interpret_uci(const vector<string> &);
};

#endif // XBOARD_PROTOCOL_H
