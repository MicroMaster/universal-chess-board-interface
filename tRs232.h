#ifndef TRS232_H
#define TRS232_H

//----------------------------------------------------------------------------
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <sys/time.h>
//----------------------------------------------------------------------------
#define COM_LEN 100
#define COM1	"/dev/ttyS0"
#define COM2	"/dev/ttyS1"
//----------------------------------------------------------------------------
#define BPS9600  0
#define BPS4800  1
#define BPS2400  2
#define BPS19200 3
//----------------------------------------------------------------------------
#define BIT8	0
#define BIT7	1
#define BIT6	2
#define BIT5	3
//----------------------------------------------------------------------------
#define STOP1	0
#define STOP2	1
//----------------------------------------------------------------------------
#define NO_PARITY  0
#define EVEN_PARITY	  1
#define ODD_PARITY 2
//----------------------------------------------------------------------------
#define WITH_STOP	0
#define NO_STOP	1
//----------------------------------------------------------------------------
#define	WITH_TIME_OUT	true
#define	NO_TIME_OUT	false
//----------------------------------------------------------------------------
#define OK		0
#define ERROR 	-1
#define TIMEOUT	-2
#define NOTHING_RECEIVED	1
//----------------------------------------------------------------------------
#include <iostream>

using namespace std;

class tRs232
{

    private:
        termios Config;
        string MessageConfigure;

        int Configure(const char *,int,int,int,int);

    protected:
        int fd;	//Descripteur de fichier

    public:
        tRs232(void);
        tRs232(const char *,int,int,int,int);
        ~tRs232();

        void display_configure_message();
        string get_configure_message();
        int send(const string&);
        int receive(int, string*);
        int receive(char, string*, int);
        int receive(char, string*,int, int);
        int receive_char(char*, int);
        int receive_file(char *, int);

};

#endif // TRS232_H
