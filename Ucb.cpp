#include "Ucb.h"

string           Ucb::com_port;
tRs232           *Ucb::serial;
Gui_protocol     *Ucb::interface;


Ucb::Ucb()
{
    //ctor
    ucb_thread=NULL;
    com_port="/dev/ttyUSB0";
}

Ucb::~Ucb()
{
    //dtor
   delete ucb_thread;
}

void Ucb::set_interface(Gui_protocol *ifc)
{
    interface = ifc;
};

void Ucb::initialize(const string &port)
{
    com_port = port;
    initialize();
}

void Ucb::initialize()
{
    serial=new tRs232((char *) com_port.c_str(),BPS9600,BIT8,NO_PARITY,STOP1);
    ucb_thread = new thread(&Ucb::ucb_driver, this);
}

void Ucb::send(const string &message)
{
    flog << "To UCB: " << message; flog.flush();
    serial->send(message+'\n');
}

void Ucb::wait(void)
{
    if (ucb_thread->joinable())
        ucb_thread->join();
}

void Ucb::ucb_driver()
{
    thread_local string buff="";
    thread_local string command="";

    flog << "Start ucb_driver" << endl; flog.flush();
    do
    {
        string output="";

        while ( (serial->receive('\n', &buff, WITH_TIME_OUT, 300)==TIMEOUT) && !interface->exit() );

        flog << "From UCB: " << buff << endl; flog.flush();
        command=buff.substr(0,buff.find('\n')-1);
        transform(command.begin(), command.end(),command.begin(), ::tolower );

        switch(command[0])
        {
        case 'm':   //  Mb7a8/Q -> move b7a8q
            output = "move " + command.substr(1,4);
            if(command.length()>6) output+=command[6];
            break;
        case 'n': // NEW GAME
            if (!interface->is_xboard_protocol()) output="New game";
            break;
        case 't': //TAKE BACK
            if (!interface->is_xboard_protocol()) output="Take back";
            break;
        case 'i': //Report ID Command
            serial->send(APPLICATION_NAME);
            output.append(APPLICATION_NAME);
            output.append(" connected on ");
            output.append(com_port);
            break;
        case 'e': //Easy Mode
            output = "Easy Mode Off";
            break;
        case 'v': //Video Mode
            serial->send("Video Mode");
            output = "Select Video Mode";
            break;
        case 'a': //Autoclock off
            serial->send("Autoclock off");
            output = "Select Autoclock off";
            break;
        case 'x': //Transmit Moves As Played
            serial->send("Xmit on");
            output = "Select Transmit Moves As Played";
            break;
        case 'p': //Board Command
            string position=interface->interpret("fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
            serial->send( (char *) position.c_str() );
            flog << "To RS: \n" << position; flog.flush();
            output = "Print Board Command";
        }
        if (output.size()>0)
        {
            flog << "To stdout: " << output << endl; flog.flush();
            if (!interface->is_xboard_protocol()) cout << '\r';
            cout << output << endl; cout.flush();
            if (!interface->is_xboard_protocol() && !interface->is_uci_protocol()) cout << "> ";
            cout.flush();
        }
    }
    while(!interface->exit());
}
