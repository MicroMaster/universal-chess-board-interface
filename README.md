# README #

### UCB Driver for Xboard ###

This a driver to use the Novag Universal Chess Board with all chess softwares implementing either xboard or uci protocol:

* Xboard protocol from [Tim Mann](http://www.tim-mann.org/)'s [xboard](http://www.gnu.org/software/xboard/).
* UCI protocol from [Shredder Computer Chess](https://www.shredderchess.com/chess-info/features/uci-universal-chess-interface.html)


### What is this repository for? ###

I am a happy user of Universal Chessboard from Novag that I connect to my PC to play against chess engines or connected users to FICS through xboard. This board gets several advantages compare with some other chessboards:

1. the price seems to be the lowest (That was the main argument when I made the choice ! )
1. A lot of LEDs to control the computer moves (no need to check the PC monitor)
1. It can be connected to some commercial softwares like fritz, hiarc...

The disadvantages:

1. the board size is quite small
1. it cannot detect the piece type on a given square. It just detect if a piece is present or not.
1. no driver available for the chess freewares

While surfing the web I found the Graham O'Neil's' web page (http://goneill.co.nz/novag) that describes this Chess Board protocol. - and I wrote the first version for MsWindows available for download at Tim Mann's page for Winboard. Now I run Linux, I've created a Linux version. I wrote it in C++, I think it should not be too difficult to run it on Windows as well. The principle is to simulate a chess program. Thus you can connect it to xboard to play with all xboard compatible engines and Internet server using the [Zippy](http://www.tim-mann.org/zippy.html) interface.

### How do I get set up? ###

* Compile the diver and do the installation:

The folowing libraries are required to link the project:

1. boost_system
1. boost_regex

DO NOT FORGET TO INSTALL THEM BEFORE PROCEEDING!


```
#!sh

   make
   sudo cp ./bin/Release/ucb /usr/local/bin
```


* Connect your Universal Chessboard (UCB) to your computer.

* Edit the file ucb.sh to set the port variable to the value that correspond to the the serial port number where the board is connected on. Ex: /dev/ttyS0 or /dev/ttyUSB0...


```
#!sh

sudo vi /usr/local/bin/ucb.sh
#!/bin/sh
ucb /dev/ttyS0

```

* Make a connection test: execute ucb alone; Once the UCB Version shows switch on the chessboard (PC position).

After a few seconds, UCB send some commands to your PC that displays there meanings. You can then make a valid move on the board and see it on your monitor. On the other way try to type in e7e5 on your keyboard and the corresponding LEDs must show on the board. Otherwise check your connection port if it corresponds to the one you declared in ucb file.

To exit from UCB just type "quit".

* Once the connection can be established it's the time to use it with xboard ! Here is an example on how to run xboard with UCB:


```
#!sh

xboard -cp -fd /usr/local/bin -fcp crafty -scp ucb.sh

```

But please refer to [xboard Documentation](http://www.gnu.org/software/xboard/manual/xboard.html) to have a complete description on how to install chess engines as UCB is perceived by xboard as a chess engine.

* For ICS usage: see the ZIPPY documentation of xboard.
I usually run:


```
#!sh

xboard -fcp ucb.sh -zp -ics -icshost freechess.org -icshelper timeseal -zippyAcceptOnly 0

```


### Contribution guidelines ###

* This program is free; you can redistribute it and/or modify it as you want.
* Please do not hesitate to send me your comments. Feel free to modify it.
* Please do not modify this header but add your remarks in the section below.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; this program is free of charge and CANNOT BE SOLD.

Thanks:

* To [Tim Mann](http://www.tim-mann.org/) for his chess website that is a reference.
* To Nick J Biggs and Graham O'Neil for their impressive work on the UCB's protocol,

### Who do I talk to? ###

* Repo owner or admin: micromaster67@yahoo.fr.
