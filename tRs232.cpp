#include "tRs232.h"

//-----------------------------------------------------------------------------
int tRs232::Configure(const char *p_port,int speed,int nb_bits,int parity,int nb_stop)
{
	fd = open(p_port,O_RDWR | O_NONBLOCK);

	if (fd == -1)
	{
   		cerr << "Error while opening " << p_port << endl;
		exit (-1);
   	}
	else
	{
	    int Res;

		MessageConfigure = "Port opening successful.";
		Config.c_lflag &= ~(ECHO) ;
		Config.c_iflag = IGNBRK | IGNPAR;
		Config.c_oflag = 0;
		Config.c_cflag = CREAD | CLOCAL;
		Config.c_cflag = Config.c_cflag & ~CRTSCTS;

		switch (speed)
		{
			case BPS9600: 	Config.c_cflag |= B9600;break;
			case BPS4800: 	Config.c_cflag |= B4800;break;
			case BPS2400: 	Config.c_cflag |= B2400;break;
			case BPS19200: 	Config.c_cflag |= B19200;break;
			default: 		cerr << "Speed not usable" << endl;
                            exit (-1);
		}

		switch (nb_bits)
		{
			case BIT8: Config.c_cflag |= CS8;break;
            case BIT7: Config.c_cflag |= CS7;break;
			case BIT6: Config.c_cflag |= CS6;break;
            case BIT5: Config.c_cflag |= CS5;break;
			default: 	cerr << "Data size error" << endl;
						exit (-1);
		}

		switch (parity)
		{
			case EVEN_PARITY:      Config.c_cflag |=  PARENB; break;
			case ODD_PARITY:    Config.c_cflag |= (PARODD | PARENB);break;
			default : break;
		}

		switch(nb_stop)
		{
			case STOP2 : Config.c_cflag |= CSTOPB;break;
		}

		Res = tcsetattr(fd, TCSANOW, &Config);
		if (Res == -1)
		{
			cerr << "Configuration error" << endl;
            exit (-1);
		}

		tcflush(fd, TCIFLUSH);
		tcflush(fd, TCOFLUSH);
	}
	return 0;
}

//----------------------------------------------------------------------------
void tRs232::display_configure_message()
{
	cout << MessageConfigure << endl << endl;
}
//----------------------------------------------------------------------------
string tRs232::get_configure_message()
{
	return(MessageConfigure);
}
//----------------------------------------------------------------------------
tRs232::tRs232(const char *p_port, int speed, int nb_bits, int parity, int nb_stop)
{
   Configure(p_port,speed,nb_bits,parity,nb_stop);
}
//----------------------------------------------------------------------------
tRs232::tRs232(void)
{
   Configure((char *) COM1,BPS9600,BIT8,NO_PARITY,STOP1);
}
//----------------------------------------------------------------------------
tRs232::~tRs232()
{
   close(fd);
}
//----------------------------------------------------------------------------
int tRs232::send(const string &pbuff)
{
   int Res = write (fd, pbuff.c_str(), pbuff.length());
   return (Res);
}
//----------------------------------------------------------------------------
int tRs232::receive(int Nb, string* received)
{
    char pbuff[COM_LEN];
	int i = 0;
	char Car;

	received->clear();
	do
	{
	    int Res = read (fd, &Car, 1);
		if (Res > 0)
			pbuff[i++] = Car;
	}
	while (i < Nb);

	pbuff[i] = '\0';
	received->append(pbuff);
	return i;
}

//----------------------------------------------------------------------------
int tRs232::receive(char c_end, string* received, int Mode)
{
    char pbuff[COM_LEN];
	int i = 0;
	char Car = 0x00;

	received->clear();
	do
	{
		int Res = read(fd, &Car, 1);
		if (Res > 0)
			pbuff[i++] = Car;
	}
	while ((Car != c_end)&&(i < (int) sizeof(pbuff)-1));

	if (Mode == NO_STOP) i--;

	pbuff[i] = '\0';
	tcflush(fd, TCIFLUSH);
	received->append(pbuff);
	return i;
}

//----------------------------------------------------------------------------
int tRs232::receive(char c_end, string *received, int Mode, int wait)
{
    char pbuff[COM_LEN];
	int i = 0;
	char Car = 0x00;
	struct timeval TimeRef, Time;
	gettimeofday(&TimeRef, NULL);
	received->clear();
	do
	{
		int Res = read(fd, &Car, 1);
		if (Res > 0)
		{
			gettimeofday(&TimeRef, NULL);
			pbuff[i++] = Car;
		}
		else
            usleep(100000);

		gettimeofday(&Time, NULL);

		unsigned int Dif = (Time.tv_sec - TimeRef.tv_sec) * 1000000 + (Time.tv_usec - TimeRef.tv_usec);
		if (Dif >= (unsigned int) wait * 1000)
		{
			pbuff[i] = '\0';
			received->append(pbuff);
			return TIMEOUT;
		}
	}
	while ((Car != c_end)&&(i < (int) sizeof(pbuff)-1));

	if (Mode == NO_STOP) i--;

	pbuff[i] = '\0';
	received->append(pbuff);
	tcflush(fd, TCIFLUSH);
	return i;
}

//----------------------------------------------------------------------------
int tRs232::receive_char(char* character, int wait)
{
	int Res;
	char Car;
	struct timeval TimeRef, Time;
	unsigned int Dif = 0;

	gettimeofday(&TimeRef, NULL);

	do
	{
		gettimeofday(&Time, NULL);
		Res = read (fd, &Car, 1);
		Dif = (Time.tv_sec - TimeRef.tv_sec) * 1000000 + (Time.tv_usec - TimeRef.tv_usec);
	}
	while((Res == 0) && ( Dif < (unsigned int) wait * 1000));

	if (Res > 0)
	{
		*character = Car;
		return OK;
	}
	return TIMEOUT;
}

//----------------------------------------------------------------------------
int tRs232::receive_file(char *filename, int wait_start)
{
	char Car;
	int Res;

	// attente du 1er caractÃ¨re du fichier
	do
		Res = receive_char(&Car, wait_start);
	while(Res != OK && Res != TIMEOUT);

	if (Res == OK)
	{
		// 1er caractÃ¨re reÃ§u
		FILE *file = fopen(filename,"w");

		do
		{
		    char chCar[2];
		    int waitc_end = 2000;

			chCar[0] = Car;
			chCar[1] = '\0';
			fputs(chCar, file);
			Res = receive_char(&Car, waitc_end);
		}
		while (Res != TIMEOUT);

		fclose(file);
		return OK;
	}
	return TIMEOUT;
}
