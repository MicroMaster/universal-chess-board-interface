#include "Gui_protocol.h"

Gui_protocol::Gui_protocol()
{
    is_exit=false;
    xboard_f=false;
    output=NULL;
}

Gui_protocol::~Gui_protocol()
{
    //dtor
}

void Gui_protocol::set_output(ostream *out)
{
    output = out;
    return;
}

void Gui_protocol::send(const string &message)
{
    flog << "To GUI: " << message << endl;
    flog.flush();
    *output << message << endl;
    output->flush();
}

bool Gui_protocol::is_move(const string *move)
{
    if ((move->length()==4 || move->length()==5) &&
            ((*move)[1]>='1') && ((*move)[1]<='8') &&
                    ((*move)[3]>='1') && ((*move)[3]<='8') &&
                        ((*move)[0]>='a') && ((*move)[0]<='h') &&
                            ((*move)[2]>='a') && ((*move)[2]<='h'))
        return true;

    return false;
}

string Gui_protocol::fen_to_ucb(const string &fen)
{
// From:
// rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1
// To:
// Position Board\n
// .8rnbqkbnr\n
// .7pppppppp\n
// .6        \n
// .5        \n
// .4    P   \n
// .3        \n
// .2PPPP PPP\n
// .1RNBQKBNR-\n
    string position="Position Board\n";
    int raw=8;
    bool nl=true;

    for (int i=0; i<(int) fen.find_first_of(" ",0); i++)
    {
        char c =  fen[i];
        if (c=='/')
        {
            nl=true;
            position += '\n';
        }
        if (nl)
        {
            position += "."+to_string(raw--);
            nl=false;
        }
        if ((c>='1') && (c<='8'))
            for (int j=1; j<=(int) (c - '0'); j++)
                position += ' ';
        else
            if (c != '/') position += c;
    }

    if (fen[fen.find_first_of(" ",0)+1]=='w')
        position += '+';
    else
        position += '-';
    position += '\n';
    return(position);
}

string Gui_protocol::fen_to_ucb2(const string &fen)
{
// From:
// rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1
// To:
// .rnbqkbnr\n
// .pppppppp\n
// .        \n
// .        \n
// .    P   \n
// .        \n
// .PPPP PPP\n
// .RNBQKBNR-\n
    string position="";
    bool nl=true;

    for (int i=0; i<(int) fen.find_first_of(" ",0); i++)
    {
        char c =  fen[i];
        if (c=='/')
        {
            nl=true;
            position += '\n';
        }
        if (nl)
        {
            position += ".";
            nl=false;
        }
        if ((c>='1') && (c<='8'))
            for (int j=1; j<=(int) (c - '0'); j++)
                position += ' ';
        else
            if (c != '/') position += c;
    }

    if (fen[fen.find_first_of(" ",0)+1]=='w')
        position += '+';
    else
        position += '-';
    position += '\n';
    return(position);
}

string Gui_protocol::beep()
{
    return (fen_to_ucb2("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq e3 0 1"));
}

bool Gui_protocol::exit()
{
    return is_exit;
}

bool Gui_protocol::is_xboard_protocol()
{
    return(xboard_f);
}

bool Gui_protocol::is_uci_protocol()
{
    return(uci_f);
}

void Gui_protocol::set_application_name(const string &name)
{
    app_name = name;
}

void Gui_protocol::tokenize(const string& str, vector<string>& tokens)
{
    split_regex(tokens, str, regex(" ") );
}

string Gui_protocol::interpret(const string &str)
{
    string input=str;

    if (is_move(&input))   // b7a8q -> Mb7-a8/Q
    {
        string move_ucb="M";

        move_ucb+=input.substr(0,2)+"-"+input.substr(2,2);

        if (input.length()==5)
        {
            transform(input.begin(), input.end(), input.begin(), ::toupper );
            move_ucb+="/";
            move_ucb+=input.substr(4,1);
        }
        return(move_ucb);
    }
    else
    {
        vector<string> command;

        tokenize(str, command);

        if (command.size()<1)
            return ("");

        if ((command[0]=="quit") || (command[0]=="exit"))
        {
            is_exit = true;
            return("");
        }

        if ((command[0]=="fen") && (command.size()>=3))
        {
            return(fen_to_ucb(command[1] + ' ' + command[2]));
        }

        if (command[0] == "new")
            //return("New Game");
            return(beep());

        if (command[0]=="xboard")
        {
            flog << "Set xboard" << endl; flog.flush();
            xboard_f=true;
            return("");
        }

        if (command[0]=="uci")
            uci_f=true;

        if (xboard_f)
            return interpret_xboard(command);

        if (uci_f)
            return interpret_uci(command);
    }
    return("");
}

string Gui_protocol::interpret_xboard(const vector<string> &command)
{

    if (command[0] == "result")
        return("New Game");

    if (command[0] == "protover")
    {
        string feature="feature ping=1 setboard=1 playother=0 san=0 usermove=0 time=0 draw=0 sigint=0 sigterm=0 reuse=1 analyze=0 myname=" +
            app_name + " variants=normal colors=0 ics=0 name=0 pause=0 nps=0 debug=0 memory=0 smp=0 done=1";
        send(feature);
        return("");
    }

    if (command[0] == "ping")
    {
        if (command.size()>1) send("pong " + command[1]);
        return("");
    }

    if ((command[0] == "setboard") && (command.size()>1))
    {
        flog << "Set board " << command[1] << endl; flog.flush();
        return(fen_to_ucb(command[1]));
    }

    return("");
}

string Gui_protocol::interpret_uci(const vector<string> &command)
{

    if (command[0] == "uci")
    {
        flog << "set uci mode" << endl; flog.flush();
        send("id name " + app_name);
        send("uciok");
        return("");
    }

    if ((command[0] == "debug") && (command.size()>1))
    {
        flog << "set debug mode to " << command[1] << endl; flog.flush();
        return("");
    }

    if (command[0] == "isready")
    {
        send("readyok");
        return("");
    }

    if ((command[0] == "setoption") && (command.size()>1))
    {
        int nb = command.size();
        string param="";
        for (int i=1; i<nb; i++) param+=command[i]+' ';
        flog << "set setoption " << param << endl; flog.flush();
        return("");
    }

    if (command[0] == "register")
    {
        int nb = command.size();
        string param="";
        for (int i=1; i<nb; i++) param+=command[i]+' ';
        flog << "Registering required " << param << endl; flog.flush();
        return("");
    }

    if (command[0] == "ucinewgame")
        return("New Game");

    if ((command[0] == "position") && (command.size()>1))
    {
        int nb = command.size();
        string param="";
        for (int i=1; i<nb; i++) param+=command[i]+' ';
        flog << "Set position to "  << param << endl; flog.flush();
        if (command[1] == "startpos")
            return("New Game");
        if ((command[1] == "fen") && (command.size()>3))
            return(fen_to_ucb(command[2] + ' ' + command[3]));
        return("");
    }

    if ((command[0] == "go") && (command.size()>1))
    {
        int nb = command.size();
        string param="";
        for (int i=1; i<nb; i++) param+=command[i]+' ';
        flog << "start calculating: " << param << endl; flog.flush();
        return("");
    }

    if (command[0] == "stop")
    {
        flog << "Stop calculating" << endl; flog.flush();
        return("");
    }

    if (command[0] == "ponderhit")
    {
        flog << "The user has played the expected move" << endl; flog.flush();
        return("");
    }

    return("");
}
