#ifndef UCB_H
#define UCB_H

#include <string>
#include <thread>
#include "tRs232.h"
#include "Gui_protocol.h"

#define BUFF_SIZE 80
#define APPLICATION_NAME "UniversalChessBoard_5.0"

using namespace std;

class Ucb
{
    public:
        Ucb(void);
        virtual ~Ucb(void);

        void            set_port(const string &);
        void            set_interface(Gui_protocol *);
        void            initialize(void);
        void            initialize(const string &);
        void            send(const string &);
        void            wait(void);

    protected:

    private:
        static string           com_port;
        static tRs232           *serial;
        static Gui_protocol     *interface;
        thread                  *ucb_thread;

        void                    ucb_driver(void);
};

#endif // UCB_H
